class Producto {
    private String nombre;
    private int demanda;
    private int horasProduccion;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDemanda(int demanda) {
        this.demanda = demanda;
    }

    public int getHorasProduccion() {
        return horasProduccion;
    }

    public void setHorasProduccion(int horasProduccion) {
        this.horasProduccion = horasProduccion;
    }

    public Producto(String nombre, int demanda, int horasProduccion) {
        this.nombre = nombre;
        this.demanda = demanda;
        this.horasProduccion = horasProduccion;
    }

    public int calcularPrecio(Ideologia ideologia)
    {
        if (ideologia == Ideologia.COMUNISTA || ideologia == Ideologia.ANARQUISTA)
        {
            return 10 * horasProduccion;
        }
        else
        {
            return 10 * demanda;
        }
    }

    public int getDemanda() {
        return demanda;
    }
}