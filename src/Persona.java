import java.util.ArrayList;
import java.util.List;
enum Ideologia {
    LIBERAL, CONSERVADORA, COMUNISTA, ANARQUISTA
}

class Persona {
    private String nombre;
    private String direccion;
    private String telefono;
    private Ideologia ideologia;
    private List<Producto> productos = new ArrayList<>();

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Ideologia getIdeologia() {
        return ideologia;
    }

    public void setIdeologia(Ideologia ideologia) {
        this.ideologia = ideologia;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    public Persona(String nombre, String direccion, String telefono, Ideologia ideologia) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.ideologia = ideologia;
    }

    public void agregarProducto(Producto unProducto) {
        productos.add(unProducto);
    }
    public int calcularNivelDemanda() {
        int nivelDemandaTotal = 0;
        for (Producto unProducto : productos) {
            nivelDemandaTotal += unProducto.getDemanda();
        }
        return nivelDemandaTotal;
    }
}